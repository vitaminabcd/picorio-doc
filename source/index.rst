.. rios_pi_doc documentation master file, created by
   sphinx-quickstart on Fri May 29 14:12:33 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PicoRio User Manual
===================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   general
   hardware
   software
